﻿using System.Data.SqlClient;
using System.Data;


namespace _2A202_21_CTP1
{
    class Database
    {
        // Declaration des objets de SGBD
        public SqlConnection connection = new SqlConnection();
        public SqlCommand cmd = new SqlCommand();
        public SqlDataAdapter dataAdapter = new SqlDataAdapter();
        
        public DataTable dataTable = new DataTable();

        // Maysla7ch n inintializihom 7it ghadi i3amro b donnée li ghadi t3tahom
        // Matalan SELECT ghada ta3tini Table 
        // Hadik Table sous forme DataReader
        // Hadchi 3lach Maghadich nsada3 rassi w initialiser bchi7aja
        // (mnin tanhdr 3la initialiser ya3ni dik l3iba dyal [new Data....()])
        public SqlDataReader dataReader;


        // Methodes
        public void Connecter()
        {
            connection.ConnectionString = "Data Source=DESKTOP-HFLAVK5;Initial Catalog=TP1;Integrated Security=True";

            if (connection.State.Equals(ConnectionState.Closed) ||
                connection.State.Equals(ConnectionState.Broken))
            {
                connection.Open();
            }
        }

        public void Deconnecter()
        {
            if (connection.State.Equals(ConnectionState.Open))
            {
                connection.Close();
            }
        }


    }
}
