﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _2A202_21_CTP1
{
    public partial class Form1 : Form
    {

        private Database db = new Database();

        public Form1()
        {
            InitializeComponent();
        }

        private void btnAjouter_Click(object sender, EventArgs e)
        {
            if (numID.Value == 0 ||
                txtNom.Text == "" ||
                txtPrenom.Text == "")
            {
                MessageBox.Show("Merci de remplir tous les champs", "Attention!!", MessageBoxButtons.OK);
                return;
            }

            db.cmd.CommandText = "INSERT INTO Etudiant values(@id, @nom, @prenom, @age)";
            db.cmd.Parameters.AddWithValue("@id", numID.Value);
            db.cmd.Parameters.AddWithValue("@nom", txtNom.Text);
            db.cmd.Parameters.AddWithValue("@prenom", txtPrenom.Text);
            db.cmd.Parameters.AddWithValue("@age", numAge.Value);

            db.cmd.ExecuteNonQuery();

            RefreshDataGrid();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            db.Connecter();
            RefreshDataGrid();
        }


        public void RefreshDataGrid()
        {
            if (db.dataTable.Rows != null)
            {
                db.dataTable.Clear();
            }

            // db.ay_attribut
            // db. flAssl dakchi lakhor aykon taybda b Majiscule
            // Gha howa m3a makaynch lwa9t
            // Madarnach Getters/Setters .. darna kolchi public
            // o7it kolchi public dakchi 3lach gha khdm b miniscule haaaaaaaaaaaaaaanya

            // Remplir data grid avec les elements de la table "Etudiant"
            db.cmd.CommandText = "Select * FROM Etudiant";

            // La commande ↥ va être utiliser avec la cnx suivante
            db.cmd.Connection = db.connection;

            // Request Select ghadi executiwha bhad la commande
            // Had cmd "ExecuteReader" Rah gha dyal SELECT
            // daba "db.dataReader " wla b7al haka
            // [ ID  | nom |  pren | age ]
            // [ ... | ... |  ...  | ... ]
            // [ ... | ... |  ...  | ... ]
            // [ ... | ... |  ...  | ... ]
            // [ ... | ... |  ...  | ... ]
            db.dataReader = db.cmd.ExecuteReader();

            // mnin tandir dataTable.Load
            // 3amar Table bdakchi li an3tik
            // dakchi li an3tik howa "db.dataReader" li howa result dyal SELECT
            db.dataTable.Load(db.dataReader);
            
            // DataGridView 3amarha bdakchi li jabna mn 3nd SELECT
            dataGridView.DataSource = db.dataTable;

            // SAD 3afak Reader
            db.dataReader.Close();
        }

    }
}
